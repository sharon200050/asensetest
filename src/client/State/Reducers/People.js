import {Types} from '../Actions/Types'
const PeopleTypes = Types.PeopleTypes

const initialState = {
  isLoading: false,
  error: null,
  data: []
};

