export const PeopleTypes = {
  ADD_PEOPLE: "ADD_PEOPLE",
  //Async action types
  FETCH_START: "FETCH_START",
  FETCH_ERROR: "FETCH_ERROR",
  FETCH_DONE: "FETCH_DONE",
};
