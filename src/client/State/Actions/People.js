import { Types } from "./Types";
import { api } from "../../Api/index";

const PeopleTypes = Types.PeopleTypes;

export const PeopleActions = {
    thunkFetchAllPeople,
    thunkFetchPereson,
    addPeople,
    fetchError,
    fetchDone,
    fetchError
}

const thunkFetchAllPeople = ({ page }) => {
  return async dispatch => {
    dispatch(fetchStart());
    const result = await api.fetchAllPeople({ page });
    if (result.error) {
      dispatch(fetchError());
      return;
    }

    dispatch(addPeople({ data: result.data }));
    dispatch(fetchDone());
  };
};

const thunkFetchPereson = ({ id }) => {
  return async dispatch => {
    dispatch(fetchStart());
    const result = await api.fetchPerson({ id });
    if (result.error) {
      dispatch(fetchError());
      return;
    }

    dispatch(addPeople({ data: [result.data] }));
    dispatch(fetchDone());
  };
};

const addPeople = data => {
  return {
    type: PeopleTypes.ADD_PEOPLE,
    data
  };
};

const fetchStart = () => {
  return {
    type: PeopleTypes.FETCH_START
  };
};

const fetchDone = () => {
  return {
    type: PeopleTypes.FETCH_DONE
  };
};

const fetchError = () => {
  return {
    type: PeopleTypes.FETCH_ERROR
  };
};
