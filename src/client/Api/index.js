const axios = require("axios");
const delay = 4000;

const fetchAllPeople = async ({page}) => {
  await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, delay);
  });

  //TODO: add error handling
  const result = await axios.get("/api/people?page=" + page);
  return result;
};

const fetchPerson = async ({id}) => {
  await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, delay);
  });

  //TODO: add error handling
  const result = await axios.get("/api/people/" + id)
  return result;
};

export const api = {
  fetchAllPeople,
  fetchPerson
};
