import React from "react";
import ReactDOM from "react-dom";
import App  from "./App";
import { createStore, applyMiddleware } from "redux";
import { rootReducer } from "./State/Reducers/index";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import logger from "redux-logger";

const createStoreWithMiddleware = applyMiddleware(thunk, logger)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(rootReducer)}>
    <App />
  </Provider>,
  document.getElementById("root")
);
