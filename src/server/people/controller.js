﻿const express = require("express");
const router = express.Router();
const service = require("./service");

// routes
router.get("/:id", getPerson);
router.get("/", getAll);

module.exports = router;

function wrapResult(result) {
  return {
    error: null,
    data: result
  };
}
function getPerson(req, res, next) {
  const id = req.params.id;

  service
    .getOne({ id })
    .then(result => {
      res.json(wrapResult(result));
    })
    .catch(err => next(err));
}

function getAll(req, res, next) {
  const page = req.params.page || 1;

  service
    .getAll({ page })
    .then(result => {
      res.json(wrapResult(result));
    })
    .catch(err => next(err));
}
