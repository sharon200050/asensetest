﻿const axios = require("axios");

module.exports = {
  getAll,
  getOne
};

const DEFAULT_PAGE = 1

async function getAll({ page }) {

  const result = await axios.get("https://swapi.co/api/people?page=" + (page || DEFAULT_PAGE));
  if (result.status === 200) {
    return result.data.results.map(person => {
      //Add id field which can be deduced from the last part
      // Of url field splitted by slash with 
      const splitted = person.url.split("/").filter(part => part.length >= 1)
      return {
        ...person,
        id: splitted[splitted.length - 1]
      }
    });
  }
}

async function getOne({id}) {
  const result = await axios.get("https://swapi.co/api/people/" + id);
  if (result.status === 200) {
    return result.data;
  }
}
